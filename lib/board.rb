require 'byebug'
class Board
  attr_accessor :grid, :row, :col
  def initialize(grid=Array.new(3){Array.new(3)})
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end


  def place_mark(pos, mark)
    self[pos] = mark.to_sym if empty?(pos)
    print "Error" unless empty?(pos)
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    (@grid + columns + diagonals).each do |mark|
      return :X if mark == Array.new(3, :X)
      return :O if mark == Array.new(3, :O)
    end
    nil
  end

  def diagonals
    diag_one = Array.new(3) {|y| self[[y, y]]}
    diag_two = [self[[0,2]], self[[1,1]], self[[2,0]]]
    [diag_one, diag_two]
  end

  def columns
    column_one = Array.new(3) {|y| self[[y, 0]]}
    column_two = Array.new(3) {|y| self[[y, 1]]}
    column_three = Array.new(3) {|y| self[[y, 2]]}
    [column_one, column_two, column_three]
  end

  def over?
    return true if @grid.flatten.all? {|y| !y.nil?} || winner
    false
  end

end
