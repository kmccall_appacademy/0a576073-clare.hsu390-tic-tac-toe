require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :current_player
  def initialize(player_one, player_two)
    player_one.mark = :X
    player_two.mark = :O
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = @player_one
  end

  def play
    play_turn until board.over?
    switch_players!
    HumanPlayer.new('').display(@board)
    puts "The winner is #{current_player.name}"
  end

  def play_turn
    @current_player.display(board)
    board.place_mark(@current_player.get_move, @current_player.mark)
    switch_players!
  end

  def switch_players!
    @current_player = current_player == @player_one ? @player_two : @player_one
  end
end

if __FILE__ == $PROGRAM_NAME
  comp = ComputerPlayer.new('computer')
  player = HumanPlayer.new('player')
  game = Game.new(player, comp)
  game.play
end
